Part 1 - The Menu (8 points)

    - Prompt the user to select one of three operations (your program must use the options 1, 2, and 3):
        1 - Drop a single chip into one slot
        2 - Drop multiple chips into one slot
        3 - Quit the program

    - After any operation is complete (except for quitting), reprompt the user to select another operation (return user to your menu)

    - If an incorrect option is entered, you must reprompt the user to select another operation (return user to your menu)

    - For Part 1, only the quit operation needs to work

    - Output the actual word "MENU" as a part of displaying your menu to the user.
    - Also, be sure to output either 
        - "SINGLE" (when they enter 1)
        - "MULTIPLE" (when they enter 2)
        - or "GOODBYE" (when the user selects 3 to quit). 
    Do this right after the user makes a valid selection.

Part 2 - Let the Chips Fall (12 points)

    - Allow the user to drop one chip into one slot
    - Prompt the user to select a slot into which he or she will drop a chip
    - If the specified slot is not between 0 and 8, immediately return the user to the menu
    - During simulation, report the path of the chip as it falls
    - Report the amount of money won for the chip

Part 3 - Bowl of Chips (12 points)

    - Allow the user to drop any number of chips into one slot
    - Prompt the user to enter the number of chips to drop
    - If the number of chips is nonpositive (including 0), immediately reprompt for valid input
    - Prompt the user to select one slot into which he or she will drop the chips
    - If the specified slot is not between 0 and 8, immediately reprompt for valid input
    - Report the total and average amount of money won for all chips
    - Do NOT report the path of each chip as it falls down the plinko board.